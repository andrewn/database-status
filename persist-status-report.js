/* eslint-disable no-console */
"use strict";

const collectResults = require("./collect-results");
const httpClient = require("./http-client");
const subDays = require("date-fns/sub_days");
const startOfDay = require("date-fns/start_of_day");
const format = require("date-fns/format");
const addDays = require("date-fns/add_days");

const PROJECT = "gitlab-com/gl-infra/infrastructure";

async function findStatusReport(since) {
  let isoSince = format(startOfDay(subDays(since, 1)), "YYYY-MM-DD");

  const statusReports = await collectResults(
    `https://gitlab.com/api/v4/projects/${encodeURIComponent(PROJECT)}/issues?labels=Status+Report&order_by=created_at&sort=desc&created_after=${isoSince}`
  );
  return statusReports[0];
}

function createNewReport(statusReport) {
  let from = format(startOfDay(statusReport.since), "YYYY-MM-DD");
  let to = format(addDays(startOfDay(statusReport.since), 7), "YYYY-MM-DD");

  const title = `Status report: ${from} - ${to}`;

  return httpClient(`https://gitlab.com/api/v4/projects/${encodeURIComponent(PROJECT)}/issues`, "POST", {
    labels: "Status Report",
    title: title,
    description: statusReport.body
  });
}

function updateExistingStatusReport(existing, statusReport) {
  if (existing.description === statusReport.body) {
    console.error("# Report description matches");
    return;
  }

  return httpClient(`https://gitlab.com/api/v4/projects/${encodeURIComponent(PROJECT)}/issues/${existing.iid}`, "PUT", { description: statusReport.body });
}

async function persistStatusReport(statusReport) {
  let existing = await findStatusReport(statusReport.since);
  if (existing) {
    await updateExistingStatusReport(existing, statusReport);
  } else {
    await createNewReport(statusReport);
  }
}

module.exports = persistStatusReport;
