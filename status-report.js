/* eslint-disable no-console */
"use strict";

const Promise = require("bluebird");
const _ = require("lodash");
const startOfWeek = require("date-fns/start_of_week");
const isToday = require("date-fns/is_today");
const subDays = require("date-fns/sub_days");
const subWeeks = require("date-fns/sub_weeks");
const getHours = require("date-fns/get_hours");
const setHours = require("date-fns/set_hours");
const startOfDay = require("date-fns/start_of_day");
const format = require("date-fns/format");
const collectResults = require("./collect-results");

const TEAM = new Set([
  "glopezfernandez",
  "andrewn",
  "dawsmith",
  "northrup",
  "alejandro",
  "dsylva",
  "yguo",
  "ctbarrett",
  "skarbek",
  "Finotto",
  "ahmadsherif",
  "abrandl",
  "aamarsanaa",
  "hphilipps",
  "peterdam",
  "T4cC0re"
]);

function startTime() {
  var now = new Date();

  var wednesday = startOfWeek(now, { weekStartsOn: 3 });

  if (isToday(wednesday)) {
    if (getHours(now) < 16) {
      wednesday = subWeeks(wednesday, 1);
    }
  }

  // Switchover time is always 16h00UTC on a Wednesday
  return setHours(wednesday, 16);
}

async function collectUniqueProjectIdsForUser(projectIdSet, username, since) {
  let isoSince = format(startOfDay(subDays(since, 1)), "YYYY-MM-DD");

  let url = `https://gitlab.com/api/v4/users/${username}/events?after=${isoSince}`;

  const events = await collectResults(url);
  for (var event of events) {
    if (event.project_id) {
      projectIdSet.add(event.project_id);
    }
  }
}

async function findTeamAuthoredMergeRequestsForProjects(since, projectIdSet) {
  let isoSince = format(startOfDay(subDays(since, 1)), "YYYY-MM-DD");

  let mergeRequests = await Promise.map(
    projectIdSet.values(),
    async projectId => {
      try {
        let mergeRequests = await collectResults(`https://gitlab.com/api/v4/projects/${projectId}/merge_requests?updated_after=${isoSince}`);

        return mergeRequests.filter(mergeRequest => {
          return TEAM.has(mergeRequest.author.username) && mergeRequest.merged_at && new Date(mergeRequest.merged_at) >= since;
        });
      } catch (e) {
        console.error("Unable to fetch merge requests:", e);
        return [];
      }
    },
    {
      concurrency: 1
    }
  );

  return _.flatten(mergeRequests);
}

async function findTeamIssuesForProjects(since, projectIdSet) {
  let isoSince = format(startOfDay(subDays(since, 1)), "YYYY-MM-DD");

  let issues = await Promise.map(
    projectIdSet.values(),
    async projectId => {
      try {
        let issues = await collectResults(`https://gitlab.com/api/v4/projects/${projectId}/issues?updated_after=${isoSince}`);

        return issues.filter(issue => {
          if (issue.confidential) return false;
          if (!issue.closed_at) return false;
          if (new Date(issue.closed_at) < since) return false;

          if (issue.assignees.length) {
            return issue.assignees.some(assignee => TEAM.has(assignee.username));
          }

          return issue.closed_by && TEAM.has(issue.closed_by.username);
        });
      } catch (e) {
        console.error("Unable to fetch issues:", e);
        return [];
      }
    },
    {
      concurrency: 5
    }
  );

  return _.flatten(issues);
}

function renderMergeRequests(mergeRequests) {
  if (!mergeRequests.length) return "";

  mergeRequests = _.sortBy(mergeRequests, mergeRequest => mergeRequest.author.username.toLowerCase());

  return (
    "## Merged Merge Requests: \n" +
    mergeRequests.map(mergeRequest => `* @${mergeRequest.author.username}: ${mergeRequest.web_url}: ${mergeRequest.title}`).join("\n")
  );
}

function attributionForIssue(issue) {
  let assignees = issue.assignees.map(assignee => `@${assignee.username}`).join(", ");
  if (assignees) return assignees;

  if (issue.closed_by) return `@${issue.closed_by.username}`;

  return "";
}

const ISSUE_WEIGHT_EMOJI = "zero|one|two|three|four|five|six|seven|eight|nine|keycap_ten".split("|");

function weightAsEmoji(issueWeight) {
  if (!issueWeight) return ":hash:";

  let emoji = ISSUE_WEIGHT_EMOJI[issueWeight];
  if (emoji) return `:${emoji}:`;

  return ":arrow_up:";
}

function renderIssueLine(issue) {
  let assignees = attributionForIssue(issue);

  let milestone = issue.milestone ? `:m: [${issue.milestone.title}](${issue.milestone.web_url}) +}` : "";
  let weight = weightAsEmoji(issue.weight);

  return `* ${weight} ${assignees}: ${issue.web_url}: ${issue.title}${milestone}`;
}

function renderIssues(issues) {
  if (!issues.length) return "";

  issues = _.sortBy(issues, issue => attributionForIssue(issue).toLowerCase());

  return "## Closed Issues: \n" + issues.map(renderIssueLine).join("\n");
}

async function getStatusReport() {
  let since = startTime();

  const projectIdSet = new Set();

  await Promise.map(TEAM, username => collectUniqueProjectIdsForUser(projectIdSet, username, since), {
    concurrency: 5
  });

  let mergeRequests = await findTeamAuthoredMergeRequestsForProjects(since, projectIdSet);
  let issues = await findTeamIssuesForProjects(since, projectIdSet);

  let renderedMergeRequests = renderMergeRequests(mergeRequests);
  let renderedIssues = renderIssues(issues);

  if (!renderedIssues && !renderedMergeRequests) {
    return;
  }

  console.log(renderedIssues, renderedMergeRequests);

  return {
    since: since,
    body: `\n# Infrastructure Group Status Update\n\n${renderedIssues}\n\n${renderedMergeRequests}\n\n-------------------------\n\nThis status report was [autogenerated](https://gitlab.com/gitlab-com/gl-infra/infra-report).\n    `
  };
}

module.exports = getStatusReport;
