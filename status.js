#!/usr/bin/env node
/* eslint-disable no-console */

"use strict";

var statusReport = require("./status-report");
var persistStatusReport = require("./persist-status-report");

statusReport()
  .then(report => {
    if (!report) return;
    return persistStatusReport(report);
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
  .then(() => {
    process.exit(0);
  });
