"use strict";

var parseLinks = require("parse-links");
var url = require("url");
var httpClient = require("./http-client");

var DEFAULT_PERPAGE = 100;

function getTotalPages(headers) {
  if (!headers.link) return;
  var links = parseLinks(headers.link);

  if (!links.last) return;
  var parsed = url.parse(links.last, true);
  return parseInt(parsed.query.page, 10);
}

async function* streamAllPages(address, stopper) {
  let urlObject = url.parse(address, true);
  delete urlObject.search;

  let query = urlObject.query;

  if (!query.per_page) query.per_page = DEFAULT_PERPAGE;
  let perPage = query.per_page;
  let initialPage = query.page || 1;
  let count = 0;

  while (true) {
    var page = initialPage + count++;
    query.page = page;

    let formattedUrl = url.format(urlObject);

    let [results, response] = await httpClient(formattedUrl);
    let lastPage = getTotalPages(response.headers) || page;

    // No results? End the stream
    if (!results || !results.length) {
      return;
    }

    for (let i = 0; i < results.length; i++) {
      let result = results[i];

      if (stopper && stopper(result)) {
        return;
      }

      yield result;
    }

    // If we get back less than one page of results,
    // or we're on the last page, end the stream
    if (results.length < perPage || page === lastPage) {
      return;
    }
  }
}

module.exports = streamAllPages;
